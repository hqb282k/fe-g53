import React from "react";
import { Helmet } from "react-helmet-async";
import { Header, Footer, MenuTab } from "Components";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
const MenuLayout = (props) => {
  const { children } = props;
  return (
    <React.Fragment>
      <Helmet>
        <title>This is Layout</title>
      </Helmet>
        <Box sx={{ display: "flex" }}>
          <MenuTab></MenuTab>
          <Box sx={{ marginLeft: "270px", width: "100%" }}>
            <Header />
            {children}
          </Box>
        </Box>
    </React.Fragment>
  );
};
export default MenuLayout;
