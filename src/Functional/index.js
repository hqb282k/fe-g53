import axios from "axios";

export const handleGetAxios = (url) => {
  return axios.get(url).then((response) => {
    return response.data;
  });
};

export const handlePostAxios = (url, body) => {
    console.log(body);
  return axios.post(url, body).then((response) => {
    return response.data;
  });
};
