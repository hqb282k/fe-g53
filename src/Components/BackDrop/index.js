import React from "react";
import Box from "@mui/material/Box";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { useDispatch, useSelector } from "react-redux";
const BackDrop = () => {
 const isShow = useSelector(state=>state.backdrop.isShow);
 console.log("gia tri backdrop: "+isShow);
    return (
    <Box
      className="wrapper-logo"
      sx={{ width: "200px", height: "70px", py: 2, margin: "0 auto" }}
    >
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isShow}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </Box>
  );
};

export default BackDrop;
