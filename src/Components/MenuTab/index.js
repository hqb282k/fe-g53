import React from "react";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import InboxIcon from "@mui/icons-material/Inbox";
import AccessibilityIcon from "@mui/icons-material/Accessibility";
import ArticleIcon from "@mui/icons-material/Article";
import LogoutIcon from "@mui/icons-material/Logout";
import Avatar from "@mui/material/Avatar";
import Tooltip from "@mui/material/Tooltip";
import { useDispatch, useSelector } from "react-redux";
import { takeToken, deleteToken } from "Redux/Slices/tokenSlice";
import { useNavigate } from "react-router-dom";

const MenuTab = (props) => {
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const style = {
    logoutButton: {
      cursor: "pointer",
      padding: "0 10px",
    },
  };

  const handleLogout = () => {
    dispatch(deleteToken());
    navigate("/");
  };

  return (
    <Box sx={{ position: "fixed", marginLeft: "10px" }}>
      <Box
        sx={{
          width: "100%",
          minWidth: 250,
          maxWidth: 250,
          background:
            "linear-gradient(195deg, rgb(66, 66, 74), rgb(25, 25, 25))",
          marginTop: "20px",
          borderRadius: "15px",
          minHeight: "95vh",
          color: "white",
        }}
      >
        <nav aria-label="main mailbox folders">
          <List>
            <ListItem disablePadding>
              <ListItemButton>
                <Avatar
                  alt="Remy Sharp"
                  src="https://cdn-icons-png.flaticon.com/512/149/149071.png"
                />
                <ListItemText sx={{ padding: "0 10px" }} primary="User" />
              </ListItemButton>
              <Tooltip title="Logout">
                <LogoutIcon
                  style={style.logoutButton}
                  onClick={() => handleLogout()}
                ></LogoutIcon>
              </Tooltip>
            </ListItem>
          </List>
        </nav>
        <Divider />
        <nav aria-label="main mailbox folders">
          <List>
            <ListItem disablePadding>
              <ListItemButton component="a" href="users">
                <ListItemIcon>
                  <AccessibilityIcon
                    sx={{ color: "white" }}
                  ></AccessibilityIcon>
                </ListItemIcon>
                <ListItemText primary="Users" />
              </ListItemButton>
            </ListItem>
            <ListItem disablePadding>
              <ListItemButton component="a" href="products">
                <ListItemIcon>
                  <ArticleIcon sx={{ color: "white" }}></ArticleIcon>
                </ListItemIcon>
                <ListItemText primary="Product" />
              </ListItemButton>
            </ListItem>
            <ListItem disablePadding>
              <ListItemButton component="a" href="slot">
                <ListItemIcon>
                  <InboxIcon sx={{ color: "white" }}></InboxIcon>
                </ListItemIcon>
                <ListItemText primary="Slot" />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
      </Box>
    </Box>
  );
};

export default MenuTab;
