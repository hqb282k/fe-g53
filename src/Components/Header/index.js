import React from "react";
import Box from "@mui/material/Box";
import images from "Assets";
const Header = () => {
  return (
    <>
      <Box
        className="wrapper-logo"
        sx={{ width: "200px", height: "70px", py: 2, margin: "0 auto" }}
      >
        <img
          src={images.logos.logoMain}
          loading="lazy"
          width="100%"
          height="100%"
        ></img>
      </Box>
    </>
  );
};

export default Header;
