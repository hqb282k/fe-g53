import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const backDropSlice = createSlice({
  name: "backDrop",
  initialState: {
    isShow: false,
  },
  reducers: {
    handleChangeStatusBackDrop: (state, action) => {
      state.isShow = action.payload.status;
    },
  },
});

export const { handleChangeStatusBackDrop } = backDropSlice.actions;

export default backDropSlice.reducer;
