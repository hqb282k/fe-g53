import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchSlots = createAsyncThunk("slots/fetchSlots", () => {
  return axios
    .get("https://62b6d176491a19c97ae9f188.mockapi.io/orders")
    .then((response) => {
      return response.data;
    });
});

export const slotSlice = createSlice({
  name: "slots",
  initialState: {
    loading: false,
    slots: [],
    error: "",
  },
  reducers: {
    deleteSlot: (state, action) => {
      console.log(action.payload);
    },
    changeStatusSlot: (state, action) => {
      console.log(action.payload);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchSlots.pending, (state) => {
      console.log("a-o");
      // Add user to the state array
      state.loading = true;
    });
    builder.addCase(fetchSlots.fulfilled, (state, action) => {
      // Add user to the state array
      console.log("b-o");
      state.loading = false;
      state.slots = action.payload;
      state.error = "";
    });
    builder.addCase(fetchSlots.rejected, (state, action) => {
      // Add user to the state array
      console.log("c-0");
      state.loading = false;
      state.slots = [];
      state.error = action.error.message;
    });
  },
});

export const { deleteSlot, changeStatusSlot } = slotSlice.actions;

export default slotSlice.reducer;
