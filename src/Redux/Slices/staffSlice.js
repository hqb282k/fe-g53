import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchStaffs = createAsyncThunk("staff/fetchStaffs", () => {
  return axios
    .get("https://62b6d176491a19c97ae9f188.mockapi.io/staff")
    .then((response) => {
      return response.data;
    });
});

export const staffSlice = createSlice({
  name: "staffs",
  initialState: {
    loading: false,
    staffs: [],
    error: "",
  },
  reducers: {
    showTask: (state, action) => {
      console.log(action.payload);
    },
    deleteStaff: (state, action) => {
      console.log(action.payload);
    },
    editStaff: (state, action) => {
      console.log(action.payload);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchStaffs.pending, (state) => {
      console.log("a");
      // Add user to the state array
      state.loading = true;
    });
    builder.addCase(fetchStaffs.fulfilled, (state, action) => {
      // Add user to the state array
      console.log("b");
      state.loading = false;
      state.staffs = action.payload;
      state.error = "";
    });
    builder.addCase(fetchStaffs.rejected, (state, action) => {
      // Add user to the state array
      console.log("c");
      state.loading = false;
      state.staffs = [];
      state.error = action.error.message;
    });
  },
});

export const { addTask, deleteStaff, editStaff } = staffSlice.actions;

export default staffSlice.reducer;
