import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { handlePostAxios } from "Functional";

export const fetchUsers = createAsyncThunk("user/fetchUsers", (state) => {
  return axios
    .get("http://localhost:8080/list", {
      headers: {
        Authorization: `Bearer ${state.token}`,
      },
    })
    .then((response) => {
      console.log(response.data);
      return response.data;
    });
});

export const userSlice = createSlice({
  name: "Users",
  initialState: {
    loading: false,
    users: [],
    error: "",
  },
  reducers: {
    deleteUser: (state, action) => {
      handlePostAxios(
        "http://localhost:8080/delete",
        {
          username: action.payload.username,
        },
      );
    },
    editUser: (state, action) => {
      console.log(action.payload);
    },
    changePasswordOfUser: (state, action) => {
      console.log(action.payload);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchUsers.pending, (state)=> {
      console.log("user-fetch");
      // Add user to the state array
      state.loading = true;
    });
    builder.addCase(fetchUsers.fulfilled, (state, action) => {
      // Add user to the state array
      console.log("user-success");
      state.loading = false;
      state.users = action.payload.data[0];
      state.error = "";
    });
    builder.addCase(fetchUsers.rejected, (state, action) => {
      // Add user to the state array
      console.log("user-end");
      state.loading = false;
      state.users = [];
      state.error = action.error.message;
    });
  },
});

export const { deleteUser, editUser,changePasswordOfUser } = userSlice.actions;

export default userSlice.reducer;
