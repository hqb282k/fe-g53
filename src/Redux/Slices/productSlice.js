import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { handlePostAxios } from "Functional";

export const fetchProducts = createAsyncThunk("user/fetchProducts", (state) => {
  return axios
    .get("http://localhost:8080/listProduct")
    .then((response) => {
      console.log(response.data);
      return response.data;
    });
});

export const productSlice = createSlice({
  name: "Users",
  initialState: {
    loading: false,
    products: [],
    error: "",
  },
  reducers: {
    deleteUser: (state, action) => {
      handlePostAxios(
        "http://localhost:8080/delete",
        {
          username: action.payload.username,
        },
      );
    },
    editUser: (state, action) => {
      console.log(action.payload);
    },
    changePasswordOfUser: (state, action) => {
      console.log(action.payload);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchProducts .pending, (state)=> {
      console.log("user-fetch");
      // Add user to the state array
      state.loading = true;
    });
    builder.addCase(fetchProducts .fulfilled, (state, action) => {
      // Add user to the state array
      console.log("user-success");
      state.loading = false;
      state.products = action.payload.data[0];
      state.error = "";
    });
    builder.addCase(fetchProducts .rejected, (state, action) => {
      // Add user to the state array
      console.log("user-end");
      state.loading = false;
      state.users = [];
      state.error = action.error.message;
    });
  },
});

export const { deleteUser, editUser,changePasswordOfUser } = productSlice.actions;

export default productSlice.reducer;
