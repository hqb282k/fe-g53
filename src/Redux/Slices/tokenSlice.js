import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { handleChangeStatusBackDrop } from "Redux/Slices/backdropSlice";
import { useDispatch, useSelector } from "react-redux";


export const fetchAuthentication = createAsyncThunk(
  "token/fetchToken",
  (account) => {
    console.log(account);
    const body = {
      username: account.user,
      password: account.password,
    };
    return axios
      .post("http://localhost:8080/auth", body)
      .then((response) => {
        console.log(response.data);
        return response.data;
      });
  }
);

export const tokenSlice = createSlice({
  name: "token",
  initialState: {
    loading: false,
    token: "",
    error: "",
  },
  reducers: {
    takeToken: (state, action) => {
      console.log(action.payload);
    },
    deleteToken: (state, action) => {
      localStorage.removeItem("token");
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchAuthentication.pending, (state) => {
      console.log("login-a");
      state.loading = true;
    });
    builder.addCase(fetchAuthentication.fulfilled, (state, action) => {
      // Add user to the state array
      console.log("login-b");
      state.token = action.payload.data.token;
      state.loading = true;
      state.error = "";
      if (localStorage.getItem("token") !== true) {
        localStorage.setItem("token", action.payload.data.token);
      }
    });
    builder.addCase(fetchAuthentication.rejected, (state, action) => {
      // Add user to the state array
      console.log("login-c");
      state.loading = false;
      state.token = "";
      state.error = action.error.message;
    });
  },
});

export const { deleteToken, takeToken } = tokenSlice.actions;

export default tokenSlice.reducer;
