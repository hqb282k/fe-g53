import { createSlice } from "@reduxjs/toolkit";
export const modalSlice = createSlice({
  name: "modal",
  initialState: {isShowing:false},
  reducers: {
    setStatusModal: (state, action) => {
     state.isShowing = action.payload.isShowing;
    },
  },
});

export const { setStatusModal } = modalSlice.actions;

export default modalSlice.reducer;
