import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const fetchOrders = createAsyncThunk("order/fetchOrders", () => {
  return axios.get("https://62b6d176491a19c97ae9f188.mockapi.io/orders").then((response) => {
    return response.data;
  });
});

export const orderSlice = createSlice({
  name: "orders",
  initialState: {
    loading: false,
    orders: [],
    error: "",
  },
  reducers: {
    deleteOrder: (state, action) => {
      console.log(action.payload);
    },
    changeStatusOrder: (state, action) => {
      console.log(action.payload);
    },
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(fetchOrders.pending, (state) => {
      console.log("a-o");
      // Add user to the state array
      state.loading = true;
    });
    builder.addCase(fetchOrders.fulfilled, (state, action) => {
      // Add user to the state array
      console.log("b-o");
      state.loading = false;
      state.orders = action.payload;
      state.error = "";
    });
    builder.addCase(fetchOrders.rejected, (state, action) => {
      // Add user to the state array
      console.log("c-0");
      state.loading = false;
      state.orders = [];
      state.error = action.error.message;
    });
  },
});

export const { deleteOrder, changeStatusOrder} = orderSlice.actions;

export default orderSlice.reducer;
