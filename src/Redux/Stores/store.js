import { configureStore } from "@reduxjs/toolkit";
import userSlice from "Redux/Slices/userSlice";
import tokenSlice from "Redux/Slices/tokenSlice";
import backdropSlice from "Redux/Slices/backdropSlice";
import modalReducer from "Redux/Slices/modalSlice";
import productSlice from "Redux/Slices/productSlice";

export default configureStore({
  reducer: {
    users: userSlice,
    token: tokenSlice,
    backdrop: backdropSlice,
    modal: modalReducer,
    products:productSlice 
  },
});
