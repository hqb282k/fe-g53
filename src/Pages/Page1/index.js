import React from "react";
import ReactDOM from "react-dom";
import { SortTable, BasicModal } from "Components";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import Grid from "@mui/material/Grid";

import {
  deleteUser,
  editUser,
  changePasswordOfUser,
  fetchUsers,
} from "Redux/Slices/userSlice";
import { setStatusModal } from "Redux/Slices/modalSlice";
import Box from "@mui/material/Box";
import images from "Assets";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const headCells = [
  {
    id: "id",
    numeric: false,
    disablePadding: true,
    label: "Id",
  },
  {
    id: "username",
    numeric: false,
    disablePadding: false,
    label: "User Name",
  },
  {
    id: "password",
    numeric: false,
    disablePadding: false,
    label: "Password",
  },
];

const ChangePassForm = () => {


  const dispatch = useDispatch();
  const handleOpenChangePassword = () => {};

  return (
    <>
      <Typography variant="h4" component="h4">
        Đổi Mật Khẩu
      </Typography>
      <Typography variant="h4" component="h4">
      </Typography>
      <Box sx={{ pb: 2, pt: 5 }}>
        <TextField
          id="new-password"
          label="Nhập mật khẩu cần đổi"
          type="password"
          autoComplete="current-password"
          variant="filled"
        />
      </Box>
      <Box sx={{ pb: 2 }}>
        <TextField
           id="confirm-password"
          label="Xác nhận mật khẩu cần đổi"
          type="password"
          autoComplete="current-password"
          variant="filled"
        />
      </Box>
      <Button variant="contained" onClick={changePasswordOfUser}>
        Xác nhận
      </Button>
    </>
  );
};

const Page1 = () => {
  const dispatch = useDispatch();
  const listUser = useSelector((state) => state.users.users);
  let data = [{}];

  const handleRemoveUser = (userNameNeedDelete) => {
    dispatch(
      deleteUser({
        username: userNameNeedDelete,
        token: localStorage.getItem("token"),
      })
    );
  };

  const handleEditUser = (id) => {
    dispatch(editUser([id]));
  };

  const handleOpenModal = (username) => {
    console.log(username)
    //Show modal have change password form with username
    dispatch(
      setStatusModal({
        isShowing: true,
      })
    );
  };

  const generateDataForTable = () => {
    listUser.map((user) => {
      const newUser = {
        id: user.id,
        username: user.username,
        password: user.password,
      };
      data.push(newUser);
    });
  };
  console.log("users: ");
  console.log(listUser);

  generateDataForTable();

  useEffect(() => {
    dispatch(fetchUsers({ token: localStorage.getItem("token") }));
  }, []);

  return (
    <>
      {data && (
        <Box sx={{ width: "100%" }}>
          <SortTable
            title={"Nhân viên"}
            data={data}
            field={headCells}
            handleRemove={handleRemoveUser}
            handleEdit={handleEditUser}
            handleOpenChangePassword={handleOpenModal}
          ></SortTable>
          <BasicModal Element={ChangePassForm}></BasicModal>
        </Box>
      )}
    </>
  );
};

export default Page1;
