import React from "react";
import ReactDOM from "react-dom";
import { SortTable } from "Components";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteOrder,
  changeStatusOrder,
  fetchOrders
} from "Redux/Slices/orderSlice";
import Box from "@mui/material/Box";
import images from "Assets";

const headCells = [
  {
    id: "id",
    numeric: false,
    disablePadding: true,
    label: "Id nhân viên",
  },
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Tên nhân viên",
  },
  {
    id: "type",
    numeric: false,
    disablePadding: false,
    label: "Chuyên kiểu tóc",
  },
  {
    id: "workAt",
    numeric: false,
    disablePadding: false,
    label: "Cơ Sở",
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
  },
];

const OrderPage = () => {
  const dispatch = useDispatch();
  const listOrder = useSelector((state) => state.orders);
  console.log(listOrder.orders);
  const handleRemoveOrder = (id) => {
    dispatch(deleteOrder([id]));
  };
  useEffect(() => {
    dispatch(fetchOrders());
  }, []);

  
  return (
    <>
      {listOrder && (
        <Box sx={{ width: "100%" }}>
          <SortTable
            title={"Order"}
            data={listOrder.orders}
            field={headCells}
            handleRemove={handleRemoveOrder}
          ></SortTable>
        </Box>
      )}
    </>
  );
};

export default OrderPage;
