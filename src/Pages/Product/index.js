import React from "react";
import ReactDOM from "react-dom";
import { SortTable, BasicModal } from "Components";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import Grid from "@mui/material/Grid";
import { fetchProducts } from "Redux/Slices/productSlice";
import { setStatusModal } from "Redux/Slices/modalSlice";
import Box from "@mui/material/Box";
import images from "Assets";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const headCells = [
  {
    id: "id",
    numeric: false,
    disablePadding: true,
    label: "Id",
  },
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Tên mặt hàng",
  },
  {
    id: "categoryy",
    numeric: false,
    disablePadding: false,
    label: "Loại Hàng",
  },
  {
    id: "price",
    numeric: false,
    disablePadding: false,
    label: "Giá",
  },
  {
    id: "image",
    numeric: false,
    disablePadding: false,
    label: "ảnh",
  },
  {
    id: "quantityy",
    numeric: false,
    disablePadding: false,
    label: "Số Lượng",
  },
];

const Product = () => {
  const dispatch = useDispatch();
  const listUser = useSelector((state) => state.products.products);
  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  return (
    <>
      {listUser && (
        <Box sx={{ width: "100%" }}>
          <SortTable
            title={"Nhân viên"}
            data={listUser}
            field={headCells}
            // handleRemove={handleRemoveUser}
            // handleEdit={handleEditUser}
            // handleOpenChangePassword={handleOpenModal}
          ></SortTable>
          {/* <BasicModal Element={ChangePassForm}></BasicModal> */}
        </Box>
      )}
    </>
  );
};

export default Product;
