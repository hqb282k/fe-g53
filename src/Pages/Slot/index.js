import React from "react";
import ReactDOM from "react-dom";
import { SortTable } from "Components";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteSlot, changeStatusSlot,fetchSlots } from "Redux/Slices/slotSlice";
import Box from "@mui/material/Box";
import images from "Assets";

const headCells = [
  {
    id: "id",
    numeric: false,
    disablePadding: true,
    label: "Id nhân viên",
  },
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Tên nhân viên",
  },
  {
    id: "type",
    numeric: false,
    disablePadding: false,
    label: "Chuyên kiểu tóc",
  },
  {
    id: "workAt",
    numeric: false,
    disablePadding: false,
    label: "Cơ Sở",
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
  },
];

const SlotPage = () => {
  const dispatch = useDispatch();
  const listSlot = useSelector((state) => state.slots);
  console.log(listSlot.slots);
  const handleRemoveSlot = (id) => {
    dispatch(deleteSlot([id]));
  };
  const handleEditSlot = (id) => {
    dispatch(changeStatusSlot([id]));
  };
  useEffect(() => {
    dispatch(fetchSlots());
  }, []);

  return (
    <>
      {listSlot && (
        <Box sx={{ width: "100%" }}>
          <SortTable
            title={"Slot"}
            data={listSlot.slots}
            field={headCells}
            handleRemove={handleRemoveSlot}
            handleEdit={handleEditSlot}
          ></SortTable>
        </Box>
      )}
    </>
  );
};

export default SlotPage;
