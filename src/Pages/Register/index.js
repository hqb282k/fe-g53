import React from "react";
import ReactDOM from "react-dom";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import axios from "axios";

const backgroundStyle = {
  background: {
    position: "relative",
    width: "100vw",
    height: "100vh",
    backgroundImage: `url(https://demos.creative-tim.com/material-dashboard-react/static/media/bg-sign-in-basic.f327db1d0e4b00ba3c81.jpeg)`,
    backgroundSize: "cover",
  },
  blur: {
    width: "100vw",
    height: "100vh",
    position: "absolute",
    backgroundColor: "rgba(255, 255, 255,0.2)",
  },
  form: {
    transform: "translate(-50%,-50%)",
    top: "50%",
    left: "50%",
    position: "absolute",
    backgroundColor: "white",
    padding: "20px 20px",
    borderRadius: "15px",
  },
  header: {
    padding: "20px 20px",
    borderRadius: "15px",
    marginTop: "-60px",
    marginBottom: "60px",
    background: "linear-gradient(195deg, rgb(73, 163, 241), rgb(26, 115, 232))",
  },
};

const validationSchema = yup.object({
  user: yup
    .string("Enter your username")
    .required("Please enter your username"),
  password: yup
    .string("Enter your password")
    .required("Please enter your password"),
});

const Register = () => {
  const navigate = useNavigate();
  const formik = useFormik({
    initialValues: {
      user: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      axios
        .post("http://localhost:8080/register", {
          username: values.user,
          password: values.password,
        })
        .then(function (response) {
          alert("Regis success");
          navigate("/users");
        })
        .catch(function (error) {
          console.log("Error");
        });
    },
  });

  return (
    <>
      {" "}
      <Box className="background" style={backgroundStyle.background}>
        <Box className="background" style={backgroundStyle.blur}></Box>
        <Box className="login-form" style={backgroundStyle.form}>
          <Box className="login-header" style={backgroundStyle.header}>
            <Typography
              variant="h5"
              sx={{
                color: "white",
                textAlign: "center",
                padding: "20px 20px",
                fontWeight: "bold",
              }}
            >
              Create New Account
            </Typography>
          </Box>
          <form onSubmit={formik.handleSubmit}>
            <TextField
              sx={{ marginBottom: "20px" }}
              fullWidth
              id="user"
              name="user"
              label="User Name"
              type="text"
              value={formik.values.userName}
              error={formik.touched.userName && Boolean(formik.errors.userName)}
              onChange={formik.handleChange}
              helperText={formik.touched.customerName && formik.errors.userName}
            />
            <TextField
              sx={{ marginBottom: "20px" }}
              fullWidth
              id="password"
              name="password"
              label="Password"
              type="password"
              value={formik.values.password}
              error={formik.touched.password && Boolean(formik.errors.password)}
              onChange={formik.handleChange}
              helperText={formik.touched.password && formik.errors.password}
            />
            <Button
              color="primary"
              variant="contained"
              fullWidth
              type="submit"
              sx={{ marginTop: "20px", fontWeight: "bold" }}
            >
              Register
            </Button>
          </form>
        </Box>
      </Box>
    </>
  );
};

export default Register;
