import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { Page1, NotFound, LoginPage,Register,Product} from "Pages";
import MenuLayout from "Layouts";

const LayoutWrapper = (Component) => {
  return (
    <MenuLayout>
      <Component />
    </MenuLayout>
  );
};

function App() {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <Helmet titleTemplate="Option" defaultTitle="Option"></Helmet>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/users" element={LayoutWrapper(Page1)} />
          <Route path="/products" element={LayoutWrapper(Product)} />
          <Route path="/register" element={<Register />} />
          <Route path="not-found" element={<NotFound />} />
          <Route
            path="*"
            element={<Navigate to="/not-found" replace={true} />}
          />
        </Routes>
      </BrowserRouter>
    </HelmetProvider>
  );
}

export default App;
